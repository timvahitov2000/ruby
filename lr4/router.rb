require_relative '../utils'

module Resource
  def connection(routes)
    if routes.nil?
      puts "No route matches for #{self}"
      return
    end

    loop do
      print 'Choose verb to interact with resources (GET/POST/PUT/DELETE) / q to exit: '
      verb = input_string
      break if verb == 'q'

      action = nil

      if verb == 'GET'
        print 'Choose action (index/show) / q to exit: '
        action = input_string
        break if action == 'q'
      end

      routes[verb][action].call if %w[index show].include?(action)

      routes[verb].call if %w[POST PUT DELETE].include?(verb)
    end
  end
end

class PostsController
  extend Resource

  def initialize
    @posts = []
  end

  def index
    if @posts.empty?
      puts 'Публикации отсутствуют'
    else
      @posts.length.times do |i|
        puts "#{i}: #{@posts[i]}"
      end
    end
  end

  def show
    print 'Введите id публикации: '
    key = input_int(true)
    if @posts.length > key
      puts "#{key}: #{@posts[key]}"
    else
      puts 'Нет записей с данным ключом'
    end
  end

  def create
    print 'Введите текст публикации: '
    text = input_string
    @posts << (text)
    puts "#{@posts.index(text)}: #{text}"
  end

  def update
    print 'Введите id публикации: '
    key = input_int(true)
    if @posts.length > key
      print 'Введите новый текст: '
      text = input_string
      @posts[key] = text
      puts "#{key}: #{text}"
    else
      puts 'Нет записей с данным ключом'
    end
  end

  def destroy
    print 'Введите id публикации: '
    key = input_int(true)
    if @posts.length > key
      @posts.delete_at(key)
    else
      puts('Нет записей с данным ключом')
    end
  end
end

class CommentsController
  extend Resource

  def initialize
    @comments = []
  end

  def index
    if @comments.empty?
      puts 'Нет комментариев'
    else
      @comments.length.times do |i|
        puts "#{i}: #{@comments[i]}"
      end
    end
  end

  def show
    print 'Введите id комментария: '
    key = input_int(true)
    if @comments.length > key
      puts "#{key}: #{@comments[key]}"
    else
      puts('Нет комментариев с данным ключом')
    end
  end

  def create
    print 'Введите текст комментария: '
    text = input_string
    @comments << (text)
    puts "#{@comments.index(text)}: #{text}"
  end

  def update
    print 'Введите id комментария: '
    key = input_int(true)
    if @comments.length > key
      print 'Введите новый текст комментария: '
      text = input_string
      @comments[key] = text
      puts "#{key}: #{text}"
    else
      puts('Нет комментариев с данным ключом')
    end
  end

  def destroy
    print 'Введите id комментария: '
    key = input_int(true)
    if @comments.length > key
      @comments.delete_at(key)
    else
      puts('Нет комментариев с данным ключом')
    end
  end
end

class Router
  def initialize
    @routes = {}
  end

  def init
    resources(PostsController, 'posts')
    resources(CommentsController, 'comments')

    loop do
      print 'Выберите ресурс для взаимодействия (1 - Posts, 2 - Comments, q - Exit): '
      choice = input_string

      PostsController.connection(@routes['posts']) if choice == '1'
      CommentsController.connection(@routes['comments']) if choice == '2'
      break if choice == 'q'
    end

    puts 'Good bye!'
  end

  def resources(klass, keyword)
    controller = klass.new
    @routes[keyword] = {
      'GET' => {
        'index' => controller.method(:index),
        'show' => controller.method(:show)
      },
      'POST' => controller.method(:create),
      'PUT' => controller.method(:update),
      'DELETE' => controller.method(:destroy)
    }
  end
end
