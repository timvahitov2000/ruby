require_relative '../utils'
require_relative '../lab3/Atm'
require_relative 'router'

def task4_1
  puts 'Так получилось, что в ЛР №3 "банкомат" изначально был реализован как класс, вследствие чего в это лабораторной был просто создан экземпляр того же класса'
  Atm.new
end

def task4_2
  Router.new.init
end

def lab4
  task4_1
  task4_2
end
