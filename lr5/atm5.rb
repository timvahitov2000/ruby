require 'socket'

class HttpAtm
  NON_VALID_MSG = 'Введите положительную сумму'.freeze
  FILE_PATH = 'lab5/balance.txt'.freeze
  START_BALANCE = 100.0
  @balance = nil

  def balance(conn)
    conn.puts("Ваш баланс: #{@balance}")
  end

  def deposit(conn, sum)
    @balance += sum
    conn.puts("Вы внесли на счёт #{sum}.")
    balance(conn)
  end

  def withdraw(conn, sum)
    if @balance >= sum
      @balance -= sum
      conn.puts("Вы сняли со счёта #{sum}")
      balance(conn)
    else
      conn.puts 'Недостаточно средств'
    end
  end

  def initialize
    @balance = (File.exist?(FILE_PATH) ? File.read(FILE_PATH) : START_BALANCE).to_f

    server = TCPServer.new('0.0.0.0', 5000)

    Thread.new do
      loop do
        connection = server.accept
        request = connection.gets
        method, full_path = request.split(' ')
        path, param = full_path.split('?')

        connection.print "HTTP/2 200\r\n"
        connection.print "Content-Type: text/html;charset=utf-8\r\n"
        connection.print "\r\n"

        case path
        when "/"
          connection.print "Помощь:<br>
/deposit?{sum} - Пополнение счёта на сумму sum<br>
/withdraw?{sum} - Снятие со счёта суммы sum<br>
/balance - Проверка баланса"
        when "/deposit"
          deposit(connection, param.to_f)
        when "/withdraw"
          withdraw(connection, param.to_f)
        when "/balance"
          balance(connection)
        else
          connection.print "Необработанный запрос:" + "\nMethod: " + method + " Path: " + path
        end

        File.write(FILE_PATH, @balance)
        connection.close

      end
    end

    puts 'Сервер запущен'

  end
end

def lab5
  HttpAtm.new
end
